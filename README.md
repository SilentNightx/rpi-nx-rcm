# rpi-nx-rcm
Open source Raspberry Pi Nintendo Switch RCM dongle built around fusee-launcher

# Fork
This fork fixes installation issues, documentation, and 100% CPU utilization.

# About
It runs on a Raspberry Pi, with all* of its internals on the /boot partition that is accessible when you insert the SD card into a Windows computer.

\* - Service folder is on rootfs on the SD card, which is not accessible by a computer running Windows

# Installation
*note:* These instructions are based around a Raspberry Pi Zero v1.3 running Raspbian Stretch Lite. Use a clean install of Raspbian found here or I can't guarantee these instructions will work: http://downloads.raspberrypi.org/raspbian_lite/images/raspbian_lite-2018-04-19/

1. Power on the Raspberry Pi and connect to the internet
2. Open a Terminal on your Raspberry Pi if there isn't already one open
3. Run `sudo bash -c "$(curl -fsSL https://gitlab.com/SilentNightx/rpi-nx-rcm/raw/master/setup.sh)"`
4. Run `sudo shutdown now`
5. Unplug the Raspberry Pi
6. Take out the SD/microSD card
7. Insert it into your desktop computer
8. Open the SD/microSD card with a file manager and navigate to /boot/nx/
9. Download a payload for RCM and move it to /nx/ folder and name it `payload.bin`
10. Eject the SD/microSD card then insert it back into the Raspberry Pi
Installation complete!

If the installation fails make sure your boot partition has enough space.

# Usage
1. Power on the Raspberry Pi
2. Wait for it to finish booting
3. Turn off your Nintendo Switch
4. Insert your jig if you don't use AutoRCM
5. Turn your Switch on while holding the volume up button or just turn it on if you have AutoRCM
6. If all goes well, the Nintendo logo SHOULDN'T appear
7. Plug the Switch into the Pi using a USB cable
8. It should boot the payload automatically within 3 seconds